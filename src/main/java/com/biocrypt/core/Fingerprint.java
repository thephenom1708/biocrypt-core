package com.biocrypt.core;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

public class Fingerprint {
    private String id;
    private String base64;

    public Fingerprint() {
        base64 = "";
        id = "";
    }

//    public Fingerprint(String id, String base64) {
//        this.id = id;
//        this.base64 = base64;
//    }

    public String getId() {
        return id;
    }

    public String getBase64() {
        return base64;
    }

    public BufferedImage image() throws IOException {
        byte[] fingerprintBytes = Base64.getDecoder().decode(base64);
        return ImageIO.read(new ByteArrayInputStream(fingerprintBytes));
    }

    public List<BufferedImage> shares(double cropRatio, int k, int n) throws IOException {
        BufferedImage fingerprintImg = image();

        int startX = (int) (fingerprintImg.getWidth() * cropRatio);
        int startY = (int) (fingerprintImg.getHeight() * cropRatio);
        int endX = (int) (fingerprintImg.getWidth() - (fingerprintImg.getWidth() * cropRatio));
        int endY = (int) (fingerprintImg.getHeight() - (fingerprintImg.getHeight() * cropRatio));

        ImageProcessor imageProcessor = new ImageProcessor(fingerprintImg, startX, startY, endX, endY);
        List<BufferedImage> shareImages = imageProcessor.generateKOutOfNShares(k, n);

        if(shareImages.size() < n) {
            int diff = n - shareImages.size();
            System.out.println(n + "  " + diff);
            for(int i = 0; i < diff; i++) {
                int position = (i % (shareImages.size() - 1)) + 1;
                shareImages.add(shareImages.get(position));
            }
        }
        return shareImages;
    }
}
