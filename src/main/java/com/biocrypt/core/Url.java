package com.biocrypt.core;

import java.util.List;

public class Url {
    public static final String SHARE_PATH = "./src/main/resources/shares/";
    public static final String GENERATED_SHARE_PATH = "./src/main/resources/generated/";
    public static final String OUTPUT_PATH = "./src/main/resources/output/";
    public static final String CURRENT_OUTPUT_PATH = "./src/main/resources/current-output/";
    public static final String INPUT_PATH = "./src/main/resources/input/";

//    public static final String REST_SERVER_IP = "localhost";
    public static final String REST_SERVER_IP = "biocrypt.herokuapp.com";
    public static final String PORT = "5000";
    public static final List<String> nodes = List.of(
//            "192.168.43.86",
//            "192.168.43.216",
//            "192.168.43.188"
//            "localhost"
            "biocrypt.herokuapp.com",
            "biocrypt-2.herokuapp.com",
            "biocrypt-3.herokuapp.com"
    );

    public static final String RETURN_SHARES_URL = "/apitest/returnShares/";
    public static final String GET_USERS_URL = "http://" + REST_SERVER_IP + ":" + PORT + "/registration/get-users/";


    public static final String DELETE_SHARES_URL = "/apitest/delete-shares/";
    public static final String DELETE_USER_MAPPINGS_URL = "/apitest/delete-user-mappings/";
    public static final String DELETE_USERS_URL = "/apitest/delete-users/";
}
