FROM gradle:6.3.0-jdk14 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:12

EXPOSE 8080

RUN mkdir /app
RUN mkdir /app/core

COPY --from=build /home/gradle/src/build/libs/*.jar /app/core/biocrypt-core.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app/core/biocrypt-core.jar"]