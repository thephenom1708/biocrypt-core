package com.biocrypt.core;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Api {
    private static final List<String> serverList = Url.nodes;

    public static void distributeShares(String userId, String fingerprintId, List<BufferedImage> shares)
            throws IOException {

        int nodeCount = serverList.size();
//        Collections.shuffle(serverList);

        int serverIndex = 0;
        String response = "";
        for (int i = 0; i < shares.size(); i++) {
            String encodedImage = Utility.convertToBase64(shares.get(i));

//            String url = "http://" + serverList.get(serverIndex) + ":" + Url.PORT + "/api/shares/post/" + userId + "/" +
//                    fingerprintId + "/";

            String url = "https://" + serverList.get(serverIndex) + "/api/shares/post/" + userId + "/" +
                    fingerprintId + "/";
            String params = "share_number=" + (i + 1) + "&" +
                    "share_data=" + URLEncoder.encode(encodedImage, StandardCharsets.UTF_8);

            System.out.println("Sending to " + serverList.get(serverIndex) + " Share number " + i + 1);

            try {
                response = Request.post(url, params);
            } catch (Exception e) {
                e.printStackTrace();
            }

            serverIndex++;
            if (serverIndex == nodeCount)
                serverIndex = 0;
        }
    }

    public static List<ShareMapping> getShareMappings(String userId, int K) {
        HashMap<String, ShareMapping> fingerprintShareMapping = new HashMap<>();

        for (String serverIp : serverList) {
//            String url = "http://" + serverIp + ":" + Url.PORT + "/api/shares/get/" + userId + "/";
            String url = "https://" + serverIp + "/api/shares/get/" + userId + "/";

            try {
                String response = Request.get(url);
                JsonArray jsonShareMappings = JsonParser.parseString(response.trim()).getAsJsonArray();
                for (JsonElement element : jsonShareMappings) {
                    String fingerprintId = element.getAsJsonObject().get("fingerprint_id").getAsString();
                    JsonArray jsonShares = element.getAsJsonObject().get("shares").getAsJsonArray();

                    List<Share> shares = new ArrayList<>();
                    for (JsonElement shareElement : jsonShares) {
                        String id = shareElement.getAsJsonObject().get("id").getAsString();
                        int number = shareElement.getAsJsonObject().get("share_number").getAsInt();
                        String data = shareElement.getAsJsonObject().get("share_data").getAsString();

                        shares.add(new Share(id, number, data));
                    }
                    if (fingerprintShareMapping.containsKey(fingerprintId)) {
                        fingerprintShareMapping.get(fingerprintId).getShares().addAll(shares);
                    } else {
                        fingerprintShareMapping.put(fingerprintId, new ShareMapping(fingerprintId, shares));
                    }
                }
                String id = fingerprintShareMapping.keySet().iterator().next();
                if(fingerprintShareMapping.get(id).getShares().size() >= K) {
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Collection<ShareMapping> shareMappingCollection = fingerprintShareMapping.values();
        return new ArrayList<>(shareMappingCollection);
    }
}
