package com.biocrypt.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiocryptApplication {
    public static void main(String[] args) {
    	SpringApplication.run(BiocryptApplication.class, args);
    }

}
