package com.biocrypt.core;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class Request {
    private final static String USER_AGENT = "Mozilla/5.0";

    private static HttpURLConnection connection(String url, String params, String requestMethod) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod(requestMethod);
        con.setRequestProperty("User-Agent", USER_AGENT);

        if(requestMethod.equalsIgnoreCase("POST")) {
            // For POST only - START
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(params.getBytes());
            os.flush();
            os.close();
            // For POST only - END
        }
        return con;
    }

    private static String response(HttpURLConnection con) throws IOException {
        int responseCode = con.getResponseCode();
        System.out.println(con.getRequestMethod() + " Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) {
            System.out.println(con.getRequestMethod() + " Request worked successfully");
        } else {
            System.out.println(con.getRequestMethod() + " Request not worked");
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    public static String get(String url) throws IOException {
        HttpURLConnection con = connection(url, null,"GET");
        return response(con);
    }

    public static String post(String url, String params) throws IOException {
        HttpURLConnection con = connection(url, params, "POST");
        return response(con);
    }

    public static JsonObject postJson(String url, String params) throws IOException {
        HttpURLConnection con = connection(url, params,"POST");
        String responseString = response(con);
        return JsonParser.parseString(responseString).getAsJsonObject();
    }

    /*public static void main(String[] args) {
        String url = "http://192.168.43.216:8080/apitest/tp/";
        String param = "name=Kaivalya";
        String response = new String();
        HttpSendData send1 = new HttpSendData(url, param);
        try
        {
            response = send1.sendPOST();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println(response);
    }*/
}
